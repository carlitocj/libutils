package br.univali.libutils;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class UtilsTest {

    public UtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test(timeout = 50, expected = IllegalArgumentException.class)
    public void testEqual1() {
        int[] a = {5, 0, 280, 1};
        Utils.equal(a, null);
    }

    @Test(timeout = 50, expected = IllegalArgumentException.class)
    public void testEqual2() {
        int[] b = {1, 5000};
        Utils.equal(null, b);
    }
    
    @Test(timeout = 50, expected = IllegalArgumentException.class)
    public void testEqual3() {
        Utils.equal(null, null);
    }

    @Test(timeout = 100)
    public void testEqual4() {
        int[] a = {5, 0, 280};
        int[] b = {5, 0, 280, 1};
        boolean result = Utils.equal(a, b);
        assertEquals(false, result);
    }

    @Test(timeout = 100)
    public void testEqual5() {
        int[] a = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] b = {1, 2, 3, 4, 5, 6, 7, 8, 8, 10};
        boolean result = Utils.equal(a, b);
        assertEquals(false, result);
    }

    @Test(timeout = 100)
    public void testEqual6() {
        int[] a = {5, 0, 280, 1};
        int[] b = {5, 0, 280, 1};
        boolean result = Utils.equal(a, b);
        assertEquals(true, result);
    }

    @Test(timeout = 50)
    public void testConcatWords1() {
        String[] words = {"concat", " ", "words"};
        String expResult = "concat words";
        String result = Utils.concatWords(words);
        assertEquals(expResult, result);
    }

    @Test(timeout = 50)
    public void testConcatWords2() {
        String[] words = {"c", "o", "n", "c", "a", "t", null, "words"};
        String expResult = "concatnullwords";
        String result = Utils.concatWords(words);
        assertEquals(expResult, result);
    }

    @Test(timeout = 50, expected = IllegalArgumentException.class)
    public void testComputeFactorial1() {
        int number = -5;
        Utils.computeFactorial(number);
    }

    @Test(timeout = 50)
    public void testComputeFactorial2() {
        int number = 10;
        String expResult = "3628800";
        String result = Utils.computeFactorial(number);
        assertEquals(expResult, result);
    }

}
